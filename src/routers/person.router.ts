import * as express from 'express';
import {PersonController} from './../controllers/person.controller'

export default class PersonRouter{
    
    public router= express.Router();

    constructor(){
        this.initializeRoutes();
    }
    public initializeRoutes(){
        this.router.get('/getAll',  PersonController.getAll);
        this.router.get('/:id', PersonController.findById);
        this.router.post('/create',PersonController.create);
        this.router.delete('/delete/:id',PersonController.deleteOne);
        this.router.put('/update/:id',PersonController.update);
    }

}
