import * as express from 'express';
import {GroupController} from './../controllers/group.conroller'

export default class GroupRouter{
    
    public router= express.Router();

    constructor(){
        this.initializeRoutes();
    }
    public initializeRoutes(){
        this.router.get('/getAll',  GroupController.getAll);
        this.router.get('/:id', GroupController.findById);
        this.router.post('/create',GroupController.create);
        this.router.delete('/delete/:id',GroupController.deleteGroup);
        this.router.put('/update/:id',GroupController.update);
        this.router.put('/update/addChild/:id',GroupController.addChild);
        this.router.put('/update/newMember/:id',GroupController.newMember);
        this.router.get('/ChildGroups/:id',GroupController.getAllChildGroups);
        this.router.get('/member/:id',GroupController.findPersonInsideGroup);
        this.router.get('/memberGroups/:id',GroupController.getAllGroupsOfPerson);
        this.router.get('/aierarchy/:id',GroupController.getHierarchy);
        
        
    }
}
