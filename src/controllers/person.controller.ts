import {Request, Response} from 'express';
import PersonManager from './../managers/person.manager';


export class PersonController{
    static getAll = async (req: Request, res: Response) =>{
        try{
            const persons= await PersonManager.getAll();
            res.send(persons  );
        }catch(err){
            res.send(err.message)
        }
    }

    static findById = async (req:Request, res:Response) =>{
        try{
            res.send(await PersonManager.findById(req.params.id));
        }catch(err){
            res.send(err.message)
        }
    }

    static create = async (req:Request, res:Response)=>{
        try{
            res.send(await PersonManager.create(req.body));
        }catch(err){
            res.send(err.message)
        }
    }

    static deleteOne = async (req:Request, res:Response) =>{
        try{
            res.send(await PersonManager.deleteOne(req.params.id));
        }catch(err){
            res.send(err.message)
        }
    }

    static update = async (req:Request, res:Response) => {
        try{
            res.send(await PersonManager.update(req.params.id, req.body));
        }catch(err){
            res.send(err.message)
        }
    }
}