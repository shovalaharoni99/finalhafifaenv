import {Request, Response} from 'express';
import GroupManager from './../managers/group.manager';
import IGroup from './../interfaces/group.interface'

export class GroupController{
    static getAll = async (req: Request, res: Response) =>{
        try{
            const groups= await GroupManager.getAll();
            res.send(groups);
        }catch(err){
            res.send(err.message)
        }
    }

    static findById = async (req:Request, res:Response) =>{
        try{
            res.send(await GroupManager.findById(req.params.id));
        }catch(err){
            res.send(err.message)
        }
    }

    static create = async (req:Request, res:Response)=>{
        try{
            res.send(await GroupManager.create(req.body));
        }catch(err){
            res.send(err.message)
        }
    }

    static deleteOne = async (req:Request, res:Response) =>{
        try{
            res.send(await GroupManager.deleteOne(req.params.id));
        }catch(err){
            res.send(err.message)
        }
    }

    static update = async (req:Request, res:Response) => {
        try{
            res.send(await GroupManager.update(req.params.id, req.body));
        }catch(err){
            res.send(err.message)
        }
    }

    static addChild = async (req:Request, res:Response) => {
        try{
            res.send(await GroupManager.addChild(req.params.id,req.body.childGroups));
        }catch(err){
            res.send(err.message);
        }
    }


    static newMember  = async (req:Request, res:Response) => {
        try{
            res.send (await GroupManager.newMember(req.params.id,req.body.person));
        }catch(err){
            res.send(err.message);
        }
    }

    static getAllChildGroups = async (req:Request, res:Response) => {
        try{
            res.send (await GroupManager.getAllChildGroups(req.params.id));
        }catch(err){
            res.send(err.message);
        }
    }

    static deleteGroup = async (req:Request, res:Response) => {
        try{
            res.send (await GroupManager.deleteGroup(req.params.id));
        }catch(err){
            res.send(err.message);
        }
    }

    static findPersonInsideGroup = async (req:Request, res:Response) => { 
        try{
            res.send (await GroupManager.findPersonInsideGroup(req.params.id,req.body.name));
        }catch(err){
            res.send(err.message);
        }
    }

    static getAllGroupsOfPerson = async (req:Request, res:Response) => { 
        try{
            res.send (await GroupManager.getAllGroupsOfPerson(req.params.id));
        }catch(err){
            res.send(err.message)
        }
    }

    static  getHierarchy= async (req:Request, res:Response) => { 
        try{
            res.send (await GroupManager.getHierarchy(req.params.id));
        }catch(err){
            res.send(err.message)
        }
    }
    
    
}