import GroupModel from './../models/group.model';
import IGroup from './../interfaces/group.interface'
import IPerson from './../interfaces/person.interface'

export default class GroupRepository {
    
    constructor () { }
    static getAll ()
    : Promise <Array<IGroup> | null> {
        return GroupModel.find({}).exec();
    }
   
    static findById (id: string) : Promise<IGroup | null> {
        return GroupModel.findById(id).exec();
    }
    
    static findByIdAndPerson (id: string) : Promise<IGroup | null> {
        return GroupModel.findById(id).populate('person').exec();
    }

    static create (group: IGroup)
    : Promise <IGroup | null> {
        return GroupModel.create(group);
    }

    static deleteOne (id: string) 
    : Promise <IGroup | null> {
        return GroupModel.findOneAndRemove({_id: id}).exec();
    }

    static update (id: string, group:IGroup) 
    : Promise<IGroup> {
        return GroupModel.update({_id: id},group).exec();
    }

    static addChild (id: string, groupId: string)
    : Promise <IGroup | null> {
        return GroupModel.updateOne({_id: id}, { $push: { childGroups: groupId } }).exec();
    }

    static newMember (id: string, personId: string)
    : Promise <IGroup |null> {
        return GroupModel.updateOne({ _id : id }, { $push: { person: personId } }).exec();
    }

    static isGroupExist(groupId: string, childId: string) 
    : Promise <Array<IGroup> |null> {
        return GroupModel.find({_id: groupId}).populate({path:"childGroups",match: {_id : childId}}).exec();
    }

    static getAllChildGroups(id: string) 
    : Promise  <Array<IGroup> |null> {
        return GroupModel.find({_id: id}).populate("childGroups").exec();
    }
    
    static findPersonInsideGroup(id: string, childId: string) 
     : Promise  <IGroup |null> {
       return GroupModel.findOne({_id: id}).populate({path:"person",match: {_id : childId}}).select(['person']).exec();
    }

    
}

    
      
    
    


