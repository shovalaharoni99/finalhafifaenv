import PersonModel from './../models/person.model';
import IPersonModel from './../interfaces/person.interface'

export default class PersonModelRepository {
    
    constructor () { }
    static getAll ()
    : Promise <Array<IPersonModel> | null> {
        return PersonModel.find({}).exec();
    }
   
    static findById (id: string) : Promise<IPersonModel | null> {
        return PersonModel.findById(id).exec();
    }

    static create (person: IPersonModel)
    : Promise <IPersonModel | null> {
        return PersonModel.create(person);
    }

    static deleteOne (id: string) 
    : Promise <IPersonModel | null> {
        return PersonModel.findOneAndRemove({id}).exec();
    }

    static update (id: string, person:IPersonModel) 
    : Promise<IPersonModel | null> {
        return PersonModel.findByIdAndUpdate(id,person).exec();
    }

    static findByName (name: string) 
    : Promise<IPersonModel | null> {
        return PersonModel.findOne({"name": name}).exec();
    }
}