import mongoose from 'mongoose';
import IPerson from '../interfaces/person.interface';

const person = new mongoose.Schema({
    name: {
         type: String,
         required:true
        },  
});

const PersonModel = mongoose.model<IPerson & mongoose.Document>('Persons', person);
export default PersonModel;