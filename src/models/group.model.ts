import mongoose from 'mongoose';
import IGroup  from '../interfaces/group.interface';

const group = new mongoose.Schema({
    name: {
         type: String,
         required:true
        },
    childGroups: [{ 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Groups',
        required :false,
        default : []
    }],
    person: [{ 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Persons',
        required :false,
        default : []
    }], 
    density: Number,
});

const GroupModel = mongoose.model<IGroup & mongoose.Document>('Groups',group);
export default GroupModel;