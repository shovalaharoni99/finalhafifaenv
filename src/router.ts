import * as express from "express";

import PersonRouter from './routers/person.router';
import GroupRouter from './routers/group.router';
let router = express.Router();

let personRouter:PersonRouter = new PersonRouter();
let groupRouter:GroupRouter = new GroupRouter();


router.use('/person', personRouter.router);
router.use('/group', groupRouter.router);


export = router;
