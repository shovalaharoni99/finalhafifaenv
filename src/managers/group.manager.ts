import IGroup from './../interfaces/group.interface'
import GroupRepository from './../repositories/group.repository';
import { Error } from 'mongoose';
import PersonRepository from './../repositories/person.repository';


export default class GroupManager{
    constructor(){}
    static async getAll (){

        return await  GroupRepository.getAll();
    }

    static async findById (id: string){
        return await  GroupRepository.findById(id);
    }

    static async create (group: IGroup){
        return await GroupRepository.create(group);
    }
    static async deleteOne (id: string){
        return await GroupRepository.deleteOne(id);
    }

    static async update (id: string, group:IGroup) {
        return await GroupRepository.update(id,group);
    }
    
    static async addChild (id: string, groupId: string) {
        try{
        if(id === groupId){
            throw new Error ("group cannot contain itself");
        }else if(await this.isGroupAlreadyContained(id,groupId)){
            throw new Error ("group is contained in the group");
        }else{
            await this.isGroupContainItself(groupId,id);
            return await GroupRepository.addChild(id,groupId);
        }
        }catch(e){
            throw e;
        }
    }

    static async newMember (id: string, personId: string){
        if(await this.isPersonContained(id,personId)){
            throw new Error ("group containe this person");
        }else{
            return await GroupRepository.newMember(id,personId);
        }
    }

    static async isGroupAlreadyContained(id: string, childId: string) { 
        let fatherGroup = await GroupRepository.findById(id);
        let flag =false;
        fatherGroup?.childGroups?.forEach(_id => {
            if(_id == childId){
                flag = true;
            }
        });
        return flag;
    }

    static async isPersonContained(id: string, personId: string) { 
        let Group = await GroupRepository.findById(id);
        let flag =false;
        Group?.person?.forEach(_id => {
            if(_id == personId){
                flag = true;
            }
        });
        return flag;
    }
    
    static async getAllChildGroups(id: string) {
        return await GroupRepository.getAllChildGroups(id);
    }

    static async deleteGroup(id: string) {
        const group = await GroupRepository.findById(id);
        group?.childGroups?.forEach(async _id => {
           await this.deleteGroup(_id);
        });
        return await GroupRepository.deleteOne(id);
      }

     

    static async isGroupContainItself(id: string, idChild: string ) {
        const group = await GroupRepository.findById(id);
        if(group?.childGroups !=undefined || group?.childGroups !=null ){
            for(const curentChild of group?.childGroups ){
            if (await this.isGroupAlreadyContained(curentChild,idChild) || (curentChild==idChild)){
                throw new Error("group contains itself");  
            }
            await this.isGroupContainItself(curentChild,idChild);
            }
        }
        return true;
    }


    static async findPersonInsideGroup(id: string, name: string) {
    
        let person = await PersonRepository.findByName(name);
        if ( person != null || person != undefined ){
            if (await this.isPersonInsideGroup(id,person?._id)){
               return await GroupRepository.findPersonInsideGroup(id,person?._id);
            }else{
                throw new Error(" person is not in that group")
            }
        }else{
            throw new Error(" there is no person that call by this name")
        }
            
        
    }

    static async getAllGroupsOfPerson (personId: string){
        let allGroups = await GroupRepository.getAll();
        let groupsOfPerson = [];
        if ( allGroups != null || allGroups != undefined ){
            for (const group of allGroups) {
                if(await this.isPersonInsideGroup(group.id,personId)){
                    groupsOfPerson.push (await GroupRepository.findById(group.id));
                }
            }
            return groupsOfPerson;
        }else{
            throw new Error(" there is no group in the system")
        }
    }
    
    static async isPersonInsideGroup (groupId: string, personId: string){
        let group = await GroupRepository.findPersonInsideGroup(groupId,personId);
        if (group?.person != null || group?.person != undefined){
            if (group?.person?.length>0){
                return true
            }else{
                return false
            }
        }
    }

    static async getAllGroupsAndPersonsInHierarchy(id: string , hierarchy: IGroup[]){  
        const group = await GroupRepository.findByIdAndPerson(id);
        console.log(await GroupRepository.findByIdAndPerson(id))
        if(group !=undefined || group !=null ){
            hierarchy.push(group);
            if(group?.childGroups  !=undefined || group?.childGroups !=null){
                for(const curentChild of group?.childGroups ){
                    console.log("dddd")
                    await this.getAllGroupsAndPersonsInHierarchy(curentChild,hierarchy );
                }
            }
        }
        return hierarchy;
        
    }

    public static async getHierarchy(id: string) {
        return await this.getAllGroupsAndPersonsInHierarchy(id, []);
      }


}
        
       

