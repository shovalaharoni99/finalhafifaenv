import IPerson from './../interfaces/person.interface'
import PersonRepository from './../repositories/person.repository';

export default class PersonManager{
    constructor(){}
    static async getAll (){

        return await  PersonRepository.getAll();
    }

    static async findById (id: string){
        return await  PersonRepository.findById(id);
    }

    static async create (person: IPerson){
        return await PersonRepository.create(person);
    }

    static async deleteOne (id: string){
        return await PersonRepository.deleteOne(id);
    }

    static async update (id: string, person:IPerson) {
        return await PersonRepository.update(id,person);
    }
}