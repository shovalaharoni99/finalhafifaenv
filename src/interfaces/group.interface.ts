interface IGroup {
    id: string;
    name: string;
    childGroups ?: string[];
    person ?: string[];
  }

  export default IGroup;