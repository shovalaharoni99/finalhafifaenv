import express from "express";
import * as bodyParser from 'body-parser'
import router from './router'
import mongoose from 'mongoose'
import { config } from "./config";

const app = express();
app.set("port",3000);
const uri:any = config.connectionDB;

mongoose.connect(uri,{useNewUrlParser:true, useUnifiedTopology: true },(err:any)=>{
    if(err) {
        console.log(err.message)
    }else{
        mongoose.set('useNewUrlParser', true);
        mongoose.set('useFindAndModify', false);
        mongoose.set('useCreateIndex', true);
        console.log("Successfully Connected");
    }
})



app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.raw());
app.use(router);
app.get('/kill',shutDown );
const server = app.listen(app.get("port"), () => {
    console.log("app is running on http://localhost:%d",app.get("port"));
})
function shutDown() {
    console.log('Received kill signal, shutting down gracefully');
    server.close();
}