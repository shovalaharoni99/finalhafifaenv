"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const person_model_1 = __importDefault(require("./../models/person.model"));
class PersonModelRepository {
    constructor() { }
    static getAll() {
        return person_model_1.default.find({}).exec();
    }
    static findById(id) {
        return person_model_1.default.findById(id).exec();
    }
    static create(person) {
        return person_model_1.default.create(person);
    }
    static deleteOne(id) {
        return person_model_1.default.findOneAndRemove({ id }).exec();
    }
    static update(id, person) {
        return person_model_1.default.findByIdAndUpdate(id, person).exec();
    }
    static findByName(name) {
        return person_model_1.default.findOne({ "name": name }).exec();
    }
}
exports.default = PersonModelRepository;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyc29uLnJlcG9zaXRvcnkuanMiLCJzb3VyY2VSb290IjoiLi9zcmMvIiwic291cmNlcyI6WyJyZXBvc2l0b3JpZXMvcGVyc29uLnJlcG9zaXRvcnkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw0RUFBbUQ7QUFHbkQsTUFBcUIscUJBQXFCO0lBRXRDLGdCQUFpQixDQUFDO0lBQ2xCLE1BQU0sQ0FBQyxNQUFNO1FBRVQsT0FBTyxzQkFBVyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2QyxDQUFDO0lBRUQsTUFBTSxDQUFDLFFBQVEsQ0FBRSxFQUFVO1FBQ3ZCLE9BQU8sc0JBQVcsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0MsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFNLENBQUUsTUFBb0I7UUFFL0IsT0FBTyxzQkFBVyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN0QyxDQUFDO0lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBRSxFQUFVO1FBRXhCLE9BQU8sc0JBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFDLEVBQUUsRUFBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDckQsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFNLENBQUUsRUFBVSxFQUFFLE1BQW1CO1FBRTFDLE9BQU8sc0JBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLEVBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0QsQ0FBQztJQUVELE1BQU0sQ0FBQyxVQUFVLENBQUUsSUFBWTtRQUUzQixPQUFPLHNCQUFXLENBQUMsT0FBTyxDQUFDLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdEQsQ0FBQztDQUNKO0FBL0JELHdDQStCQyJ9