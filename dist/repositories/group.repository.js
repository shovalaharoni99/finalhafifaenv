"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const group_model_1 = __importDefault(require("./../models/group.model"));
class GroupRepository {
    constructor() { }
    static getAll() {
        return group_model_1.default.find({}).exec();
    }
    static findById(id) {
        return group_model_1.default.findById(id).exec();
    }
    static findByIdAndPerson(id) {
        return group_model_1.default.findById(id).populate('person').exec();
    }
    static create(group) {
        return group_model_1.default.create(group);
    }
    static deleteOne(id) {
        return group_model_1.default.findOneAndRemove({ _id: id }).exec();
    }
    static update(id, group) {
        return group_model_1.default.update({ _id: id }, group).exec();
    }
    static addChild(id, groupId) {
        return group_model_1.default.updateOne({ _id: id }, { $push: { childGroups: groupId } }).exec();
    }
    static newMember(id, personId) {
        return group_model_1.default.updateOne({ _id: id }, { $push: { person: personId } }).exec();
    }
    static isGroupExist(groupId, childId) {
        return group_model_1.default.find({ _id: groupId }).populate({ path: "childGroups", match: { _id: childId } }).exec();
    }
    static getAllChildGroups(id) {
        return group_model_1.default.find({ _id: id }).populate("childGroups").exec();
    }
    static findPersonInsideGroup(id, childId) {
        return group_model_1.default.findOne({ _id: id }).populate({ path: "person", match: { _id: childId } }).select(['person']).exec();
    }
}
exports.default = GroupRepository;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAucmVwb3NpdG9yeS5qcyIsInNvdXJjZVJvb3QiOiIuL3NyYy8iLCJzb3VyY2VzIjpbInJlcG9zaXRvcmllcy9ncm91cC5yZXBvc2l0b3J5LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsMEVBQWlEO0FBSWpELE1BQXFCLGVBQWU7SUFFaEMsZ0JBQWlCLENBQUM7SUFDbEIsTUFBTSxDQUFDLE1BQU07UUFFVCxPQUFPLHFCQUFVLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3RDLENBQUM7SUFFRCxNQUFNLENBQUMsUUFBUSxDQUFFLEVBQVU7UUFDdkIsT0FBTyxxQkFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMxQyxDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFFLEVBQVU7UUFDaEMsT0FBTyxxQkFBVSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDN0QsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFNLENBQUUsS0FBYTtRQUV4QixPQUFPLHFCQUFVLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRCxNQUFNLENBQUMsU0FBUyxDQUFFLEVBQVU7UUFFeEIsT0FBTyxxQkFBVSxDQUFDLGdCQUFnQixDQUFDLEVBQUMsR0FBRyxFQUFFLEVBQUUsRUFBQyxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDekQsQ0FBQztJQUVELE1BQU0sQ0FBQyxNQUFNLENBQUUsRUFBVSxFQUFFLEtBQVk7UUFFbkMsT0FBTyxxQkFBVSxDQUFDLE1BQU0sQ0FBQyxFQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUMsRUFBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNyRCxDQUFDO0lBRUQsTUFBTSxDQUFDLFFBQVEsQ0FBRSxFQUFVLEVBQUUsT0FBZTtRQUV4QyxPQUFPLHFCQUFVLENBQUMsU0FBUyxDQUFDLEVBQUMsR0FBRyxFQUFFLEVBQUUsRUFBQyxFQUFFLEVBQUUsS0FBSyxFQUFFLEVBQUUsV0FBVyxFQUFFLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2RixDQUFDO0lBRUQsTUFBTSxDQUFDLFNBQVMsQ0FBRSxFQUFVLEVBQUUsUUFBZ0I7UUFFMUMsT0FBTyxxQkFBVSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEdBQUcsRUFBRyxFQUFFLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDdEYsQ0FBQztJQUVELE1BQU0sQ0FBQyxZQUFZLENBQUMsT0FBZSxFQUFFLE9BQWU7UUFFaEQsT0FBTyxxQkFBVSxDQUFDLElBQUksQ0FBQyxFQUFDLEdBQUcsRUFBRSxPQUFPLEVBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFDLElBQUksRUFBQyxhQUFhLEVBQUMsS0FBSyxFQUFFLEVBQUMsR0FBRyxFQUFHLE9BQU8sRUFBQyxFQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN4RyxDQUFDO0lBRUQsTUFBTSxDQUFDLGlCQUFpQixDQUFDLEVBQVU7UUFFL0IsT0FBTyxxQkFBVSxDQUFDLElBQUksQ0FBQyxFQUFDLEdBQUcsRUFBRSxFQUFFLEVBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNyRSxDQUFDO0lBRUQsTUFBTSxDQUFDLHFCQUFxQixDQUFDLEVBQVUsRUFBRSxPQUFlO1FBRXJELE9BQU8scUJBQVUsQ0FBQyxPQUFPLENBQUMsRUFBQyxHQUFHLEVBQUUsRUFBRSxFQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLEtBQUssRUFBRSxFQUFDLEdBQUcsRUFBRyxPQUFPLEVBQUMsRUFBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNuSCxDQUFDO0NBR0o7QUF6REQsa0NBeURDIn0=