"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const group = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true
    },
    childGroups: [{
            type: mongoose_1.default.Schema.Types.ObjectId,
            ref: 'Groups',
            required: false,
            default: []
        }],
    person: [{
            type: mongoose_1.default.Schema.Types.ObjectId,
            ref: 'Persons',
            required: false,
            default: []
        }],
    density: Number,
});
const GroupModel = mongoose_1.default.model('Groups', group);
exports.default = GroupModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAubW9kZWwuanMiLCJzb3VyY2VSb290IjoiLi9zcmMvIiwic291cmNlcyI6WyJtb2RlbHMvZ3JvdXAubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSx3REFBZ0M7QUFHaEMsTUFBTSxLQUFLLEdBQUcsSUFBSSxrQkFBUSxDQUFDLE1BQU0sQ0FBQztJQUM5QixJQUFJLEVBQUU7UUFDRCxJQUFJLEVBQUUsTUFBTTtRQUNaLFFBQVEsRUFBQyxJQUFJO0tBQ2I7SUFDTCxXQUFXLEVBQUUsQ0FBQztZQUNWLElBQUksRUFBRSxrQkFBUSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUTtZQUNwQyxHQUFHLEVBQUUsUUFBUTtZQUNiLFFBQVEsRUFBRSxLQUFLO1lBQ2YsT0FBTyxFQUFHLEVBQUU7U0FDZixDQUFDO0lBQ0YsTUFBTSxFQUFFLENBQUM7WUFDTCxJQUFJLEVBQUUsa0JBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVE7WUFDcEMsR0FBRyxFQUFFLFNBQVM7WUFDZCxRQUFRLEVBQUUsS0FBSztZQUNmLE9BQU8sRUFBRyxFQUFFO1NBQ2YsQ0FBQztJQUNGLE9BQU8sRUFBRSxNQUFNO0NBQ2xCLENBQUMsQ0FBQztBQUVILE1BQU0sVUFBVSxHQUFHLGtCQUFRLENBQUMsS0FBSyxDQUE2QixRQUFRLEVBQUMsS0FBSyxDQUFDLENBQUM7QUFDOUUsa0JBQWUsVUFBVSxDQUFDIn0=