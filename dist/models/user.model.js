"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const person = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true
    }
});
const Person = mongoose_1.default.model('Person', person);
exports.default = Person;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiIuL3NyYy8iLCJzb3VyY2VzIjpbIm1vZGVscy91c2VyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsd0RBQWdDO0FBR2hDLE1BQU0sTUFBTSxHQUFHLElBQUksa0JBQVEsQ0FBQyxNQUFNLENBQUM7SUFDL0IsSUFBSSxFQUFFO1FBQ0QsSUFBSSxFQUFFLE1BQU07UUFDWixRQUFRLEVBQUMsSUFBSTtLQUNiO0NBQ1IsQ0FBQyxDQUFDO0FBRUgsTUFBTSxNQUFNLEdBQUcsa0JBQVEsQ0FBQyxLQUFLLENBQTRCLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztBQUMzRSxrQkFBZSxNQUFNLENBQUMifQ==