"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const person = new mongoose_1.default.Schema({
    name: {
        type: String,
        required: true
    },
});
const PersonModel = mongoose_1.default.model('Persons', person);
exports.default = PersonModel;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyc29uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Ii4vc3JjLyIsInNvdXJjZXMiOlsibW9kZWxzL3BlcnNvbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLHdEQUFnQztBQUdoQyxNQUFNLE1BQU0sR0FBRyxJQUFJLGtCQUFRLENBQUMsTUFBTSxDQUFDO0lBQy9CLElBQUksRUFBRTtRQUNELElBQUksRUFBRSxNQUFNO1FBQ1osUUFBUSxFQUFDLElBQUk7S0FDYjtDQUNSLENBQUMsQ0FBQztBQUVILE1BQU0sV0FBVyxHQUFHLGtCQUFRLENBQUMsS0FBSyxDQUE4QixTQUFTLEVBQUUsTUFBTSxDQUFDLENBQUM7QUFDbkYsa0JBQWUsV0FBVyxDQUFDIn0=