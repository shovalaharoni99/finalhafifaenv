"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const uri = 'mongodb://127.0.0.1:27017/local';
mongoose_1.default.connect(uri, (err) => {
    if (err) {
        console.log(err.message);
    }
    else {
        console.log("Successfully Connected");
    }
});
exports.BooksSchema = new mongoose_1.default.Schema({
    title: { type: String, require: true },
    author: { type: String, required: true }
});
const Book = mongoose_1.default.model('Booke', exports.BooksSchema);
exports.default = Book;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vay5qcyIsInNvdXJjZVJvb3QiOiIuL3NyYy8iLCJzb3VyY2VzIjpbImJvb2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSx3REFBK0I7QUFFL0IsTUFBTSxHQUFHLEdBQVcsaUNBQWlDLENBQUM7QUFFdEQsa0JBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFDLENBQUMsR0FBTyxFQUFDLEVBQUU7SUFDNUIsSUFBRyxHQUFHLEVBQUU7UUFDSixPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtLQUMzQjtTQUFJO1FBQ0QsT0FBTyxDQUFDLEdBQUcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0tBQ3pDO0FBQ0wsQ0FBQyxDQUFDLENBQUE7QUFFVyxRQUFBLFdBQVcsR0FBRyxJQUFJLGtCQUFRLENBQUMsTUFBTSxDQUFDO0lBQzNDLEtBQUssRUFBRSxFQUFDLElBQUksRUFBRSxNQUFNLEVBQUcsT0FBTyxFQUFDLElBQUksRUFBQztJQUNwQyxNQUFNLEVBQUUsRUFBQyxJQUFJLEVBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUM7Q0FDeEMsQ0FBQyxDQUFDO0FBRUgsTUFBTSxJQUFJLEdBQUcsa0JBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFDLG1CQUFXLENBQUUsQ0FBQztBQUNsRCxrQkFBZSxJQUFJLENBQUMifQ==