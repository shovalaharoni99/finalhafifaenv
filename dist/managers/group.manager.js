"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const group_repository_1 = __importDefault(require("./../repositories/group.repository"));
const mongoose_1 = require("mongoose");
const person_repository_1 = __importDefault(require("./../repositories/person.repository"));
class GroupManager {
    constructor() { }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield group_repository_1.default.getAll();
        });
    }
    static findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield group_repository_1.default.findById(id);
        });
    }
    static create(group) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield group_repository_1.default.create(group);
        });
    }
    static deleteOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield group_repository_1.default.deleteOne(id);
        });
    }
    static update(id, group) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield group_repository_1.default.update(id, group);
        });
    }
    static addChild(id, groupId) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (id === groupId) {
                    throw new mongoose_1.Error("group cannot contain itself");
                }
                else if (yield this.isGroupAlreadyContained(id, groupId)) {
                    throw new mongoose_1.Error("group is contained in the group");
                }
                else {
                    yield this.isGroupContainItself(groupId, id);
                    return yield group_repository_1.default.addChild(id, groupId);
                }
            }
            catch (e) {
                throw e;
            }
        });
    }
    static newMember(id, personId) {
        return __awaiter(this, void 0, void 0, function* () {
            if (yield this.isPersonContained(id, personId)) {
                throw new mongoose_1.Error("group containe this person");
            }
            else {
                return yield group_repository_1.default.newMember(id, personId);
            }
        });
    }
    static isGroupAlreadyContained(id, childId) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            let fatherGroup = yield group_repository_1.default.findById(id);
            let flag = false;
            (_a = fatherGroup === null || fatherGroup === void 0 ? void 0 : fatherGroup.childGroups) === null || _a === void 0 ? void 0 : _a.forEach(_id => {
                if (_id == childId) {
                    flag = true;
                }
            });
            return flag;
        });
    }
    static isPersonContained(id, personId) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            let Group = yield group_repository_1.default.findById(id);
            let flag = false;
            (_a = Group === null || Group === void 0 ? void 0 : Group.person) === null || _a === void 0 ? void 0 : _a.forEach(_id => {
                if (_id == personId) {
                    flag = true;
                }
            });
            return flag;
        });
    }
    static getAllChildGroups(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield group_repository_1.default.getAllChildGroups(id);
        });
    }
    static deleteGroup(id) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const group = yield group_repository_1.default.findById(id);
            (_a = group === null || group === void 0 ? void 0 : group.childGroups) === null || _a === void 0 ? void 0 : _a.forEach((_id) => __awaiter(this, void 0, void 0, function* () {
                yield this.deleteGroup(_id);
            }));
            return yield group_repository_1.default.deleteOne(id);
        });
    }
    static isGroupContainItself(id, idChild) {
        return __awaiter(this, void 0, void 0, function* () {
            const group = yield group_repository_1.default.findById(id);
            if ((group === null || group === void 0 ? void 0 : group.childGroups) != undefined || (group === null || group === void 0 ? void 0 : group.childGroups) != null) {
                for (const curentChild of group === null || group === void 0 ? void 0 : group.childGroups) {
                    if ((yield this.isGroupAlreadyContained(curentChild, idChild)) || (curentChild == idChild)) {
                        throw new mongoose_1.Error("group contains itself");
                    }
                    yield this.isGroupContainItself(curentChild, idChild);
                }
            }
            return true;
        });
    }
    static findPersonInsideGroup(id, name) {
        return __awaiter(this, void 0, void 0, function* () {
            let person = yield person_repository_1.default.findByName(name);
            if (person != null || person != undefined) {
                if (yield this.isPersonInsideGroup(id, person === null || person === void 0 ? void 0 : person._id)) {
                    return yield group_repository_1.default.findPersonInsideGroup(id, person === null || person === void 0 ? void 0 : person._id);
                }
                else {
                    throw new mongoose_1.Error(" person is not in that group");
                }
            }
            else {
                throw new mongoose_1.Error(" there is no person that call by this name");
            }
        });
    }
    static getAllGroupsOfPerson(personId) {
        return __awaiter(this, void 0, void 0, function* () {
            let allGroups = yield group_repository_1.default.getAll();
            let groupsOfPerson = [];
            if (allGroups != null || allGroups != undefined) {
                for (const group of allGroups) {
                    if (yield this.isPersonInsideGroup(group.id, personId)) {
                        groupsOfPerson.push(yield group_repository_1.default.findById(group.id));
                    }
                }
                return groupsOfPerson;
            }
            else {
                throw new mongoose_1.Error(" there is no group in the system");
            }
        });
    }
    static isPersonInsideGroup(groupId, personId) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            let group = yield group_repository_1.default.findPersonInsideGroup(groupId, personId);
            if ((group === null || group === void 0 ? void 0 : group.person) != null || (group === null || group === void 0 ? void 0 : group.person) != undefined) {
                if (((_a = group === null || group === void 0 ? void 0 : group.person) === null || _a === void 0 ? void 0 : _a.length) > 0) {
                    return true;
                }
                else {
                    return false;
                }
            }
        });
    }
    static getAllGroupsAndPersonsInHierarchy(id, hierarchy) {
        return __awaiter(this, void 0, void 0, function* () {
            const group = yield group_repository_1.default.findByIdAndPerson(id);
            console.log(yield group_repository_1.default.findByIdAndPerson(id));
            if (group != undefined || group != null) {
                hierarchy.push(group);
                if ((group === null || group === void 0 ? void 0 : group.childGroups) != undefined || (group === null || group === void 0 ? void 0 : group.childGroups) != null) {
                    for (const curentChild of group === null || group === void 0 ? void 0 : group.childGroups) {
                        console.log("dddd");
                        yield this.getAllGroupsAndPersonsInHierarchy(curentChild, hierarchy);
                    }
                }
            }
            return hierarchy;
        });
    }
    static getHierarchy(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getAllGroupsAndPersonsInHierarchy(id, []);
        });
    }
}
exports.default = GroupManager;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAubWFuYWdlci5qcyIsInNvdXJjZVJvb3QiOiIuL3NyYy8iLCJzb3VyY2VzIjpbIm1hbmFnZXJzL2dyb3VwLm1hbmFnZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFDQSwwRkFBaUU7QUFDakUsdUNBQWlDO0FBQ2pDLDRGQUFtRTtBQUduRSxNQUFxQixZQUFZO0lBQzdCLGdCQUFjLENBQUM7SUFDZixNQUFNLENBQU8sTUFBTTs7WUFFZixPQUFPLE1BQU8sMEJBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztRQUMzQyxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQU8sUUFBUSxDQUFFLEVBQVU7O1lBQzdCLE9BQU8sTUFBTywwQkFBZSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMvQyxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQU8sTUFBTSxDQUFFLEtBQWE7O1lBQzlCLE9BQU8sTUFBTSwwQkFBZSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvQyxDQUFDO0tBQUE7SUFDRCxNQUFNLENBQU8sU0FBUyxDQUFFLEVBQVU7O1lBQzlCLE9BQU8sTUFBTSwwQkFBZSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMvQyxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQU8sTUFBTSxDQUFFLEVBQVUsRUFBRSxLQUFZOztZQUN6QyxPQUFPLE1BQU0sMEJBQWUsQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ2xELENBQUM7S0FBQTtJQUVELE1BQU0sQ0FBTyxRQUFRLENBQUUsRUFBVSxFQUFFLE9BQWU7O1lBQzlDLElBQUc7Z0JBQ0gsSUFBRyxFQUFFLEtBQUssT0FBTyxFQUFDO29CQUNkLE1BQU0sSUFBSSxnQkFBSyxDQUFFLDZCQUE2QixDQUFDLENBQUM7aUJBQ25EO3FCQUFLLElBQUcsTUFBTSxJQUFJLENBQUMsdUJBQXVCLENBQUMsRUFBRSxFQUFDLE9BQU8sQ0FBQyxFQUFDO29CQUNwRCxNQUFNLElBQUksZ0JBQUssQ0FBRSxpQ0FBaUMsQ0FBQyxDQUFDO2lCQUN2RDtxQkFBSTtvQkFDRCxNQUFNLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLEVBQUMsRUFBRSxDQUFDLENBQUM7b0JBQzVDLE9BQU8sTUFBTSwwQkFBZSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUMsT0FBTyxDQUFDLENBQUM7aUJBQ3JEO2FBQ0E7WUFBQSxPQUFNLENBQUMsRUFBQztnQkFDTCxNQUFNLENBQUMsQ0FBQzthQUNYO1FBQ0wsQ0FBQztLQUFBO0lBRUQsTUFBTSxDQUFPLFNBQVMsQ0FBRSxFQUFVLEVBQUUsUUFBZ0I7O1lBQ2hELElBQUcsTUFBTSxJQUFJLENBQUMsaUJBQWlCLENBQUMsRUFBRSxFQUFDLFFBQVEsQ0FBQyxFQUFDO2dCQUN6QyxNQUFNLElBQUksZ0JBQUssQ0FBRSw0QkFBNEIsQ0FBQyxDQUFDO2FBQ2xEO2lCQUFJO2dCQUNELE9BQU8sTUFBTSwwQkFBZSxDQUFDLFNBQVMsQ0FBQyxFQUFFLEVBQUMsUUFBUSxDQUFDLENBQUM7YUFDdkQ7UUFDTCxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQU8sdUJBQXVCLENBQUMsRUFBVSxFQUFFLE9BQWU7OztZQUM1RCxJQUFJLFdBQVcsR0FBRyxNQUFNLDBCQUFlLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ3JELElBQUksSUFBSSxHQUFFLEtBQUssQ0FBQztZQUNoQixNQUFBLFdBQVcsYUFBWCxXQUFXLHVCQUFYLFdBQVcsQ0FBRSxXQUFXLDBDQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDcEMsSUFBRyxHQUFHLElBQUksT0FBTyxFQUFDO29CQUNkLElBQUksR0FBRyxJQUFJLENBQUM7aUJBQ2Y7WUFDTCxDQUFDLEVBQUU7WUFDSCxPQUFPLElBQUksQ0FBQzs7S0FDZjtJQUVELE1BQU0sQ0FBTyxpQkFBaUIsQ0FBQyxFQUFVLEVBQUUsUUFBZ0I7OztZQUN2RCxJQUFJLEtBQUssR0FBRyxNQUFNLDBCQUFlLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQy9DLElBQUksSUFBSSxHQUFFLEtBQUssQ0FBQztZQUNoQixNQUFBLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxNQUFNLDBDQUFFLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDekIsSUFBRyxHQUFHLElBQUksUUFBUSxFQUFDO29CQUNmLElBQUksR0FBRyxJQUFJLENBQUM7aUJBQ2Y7WUFDTCxDQUFDLEVBQUU7WUFDSCxPQUFPLElBQUksQ0FBQzs7S0FDZjtJQUVELE1BQU0sQ0FBTyxpQkFBaUIsQ0FBQyxFQUFVOztZQUNyQyxPQUFPLE1BQU0sMEJBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN2RCxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQU8sV0FBVyxDQUFDLEVBQVU7OztZQUMvQixNQUFNLEtBQUssR0FBRyxNQUFNLDBCQUFlLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1lBQ2pELE1BQUEsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLFdBQVcsMENBQUUsT0FBTyxDQUFDLENBQU0sR0FBRyxFQUFDLEVBQUU7Z0JBQ3JDLE1BQU0sSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUMvQixDQUFDLENBQUEsRUFBRTtZQUNILE9BQU8sTUFBTSwwQkFBZSxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQzs7S0FDNUM7SUFJSCxNQUFNLENBQU8sb0JBQW9CLENBQUMsRUFBVSxFQUFFLE9BQWU7O1lBQ3pELE1BQU0sS0FBSyxHQUFHLE1BQU0sMEJBQWUsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDakQsSUFBRyxDQUFBLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxXQUFXLEtBQUcsU0FBUyxJQUFJLENBQUEsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLFdBQVcsS0FBRyxJQUFJLEVBQUU7Z0JBQzVELEtBQUksTUFBTSxXQUFXLElBQUksS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLFdBQVcsRUFBRTtvQkFDN0MsSUFBSSxDQUFBLE1BQU0sSUFBSSxDQUFDLHVCQUF1QixDQUFDLFdBQVcsRUFBQyxPQUFPLENBQUMsS0FBSSxDQUFDLFdBQVcsSUFBRSxPQUFPLENBQUMsRUFBQzt3QkFDbEYsTUFBTSxJQUFJLGdCQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQztxQkFDNUM7b0JBQ0QsTUFBTSxJQUFJLENBQUMsb0JBQW9CLENBQUMsV0FBVyxFQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUNwRDthQUNKO1lBQ0QsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQztLQUFBO0lBR0QsTUFBTSxDQUFPLHFCQUFxQixDQUFDLEVBQVUsRUFBRSxJQUFZOztZQUV2RCxJQUFJLE1BQU0sR0FBRyxNQUFNLDJCQUFnQixDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNyRCxJQUFLLE1BQU0sSUFBSSxJQUFJLElBQUksTUFBTSxJQUFJLFNBQVMsRUFBRTtnQkFDeEMsSUFBSSxNQUFNLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQUMsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLEdBQUcsQ0FBQyxFQUFDO29CQUNoRCxPQUFPLE1BQU0sMEJBQWUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLEVBQUMsTUFBTSxhQUFOLE1BQU0sdUJBQU4sTUFBTSxDQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUNyRTtxQkFBSTtvQkFDRCxNQUFNLElBQUksZ0JBQUssQ0FBQyw4QkFBOEIsQ0FBQyxDQUFBO2lCQUNsRDthQUNKO2lCQUFJO2dCQUNELE1BQU0sSUFBSSxnQkFBSyxDQUFDLDRDQUE0QyxDQUFDLENBQUE7YUFDaEU7UUFHTCxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQU8sb0JBQW9CLENBQUUsUUFBZ0I7O1lBQy9DLElBQUksU0FBUyxHQUFHLE1BQU0sMEJBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUMvQyxJQUFJLGNBQWMsR0FBRyxFQUFFLENBQUM7WUFDeEIsSUFBSyxTQUFTLElBQUksSUFBSSxJQUFJLFNBQVMsSUFBSSxTQUFTLEVBQUU7Z0JBQzlDLEtBQUssTUFBTSxLQUFLLElBQUksU0FBUyxFQUFFO29CQUMzQixJQUFHLE1BQU0sSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUMsUUFBUSxDQUFDLEVBQUM7d0JBQ2pELGNBQWMsQ0FBQyxJQUFJLENBQUUsTUFBTSwwQkFBZSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztxQkFDbEU7aUJBQ0o7Z0JBQ0QsT0FBTyxjQUFjLENBQUM7YUFDekI7aUJBQUk7Z0JBQ0QsTUFBTSxJQUFJLGdCQUFLLENBQUMsa0NBQWtDLENBQUMsQ0FBQTthQUN0RDtRQUNMLENBQUM7S0FBQTtJQUVELE1BQU0sQ0FBTyxtQkFBbUIsQ0FBRSxPQUFlLEVBQUUsUUFBZ0I7OztZQUMvRCxJQUFJLEtBQUssR0FBRyxNQUFNLDBCQUFlLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQSxLQUFLLGFBQUwsS0FBSyx1QkFBTCxLQUFLLENBQUUsTUFBTSxLQUFJLElBQUksSUFBSSxDQUFBLEtBQUssYUFBTCxLQUFLLHVCQUFMLEtBQUssQ0FBRSxNQUFNLEtBQUksU0FBUyxFQUFDO2dCQUNwRCxJQUFJLE9BQUEsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLE1BQU0sMENBQUUsTUFBTSxJQUFDLENBQUMsRUFBQztvQkFDeEIsT0FBTyxJQUFJLENBQUE7aUJBQ2Q7cUJBQUk7b0JBQ0QsT0FBTyxLQUFLLENBQUE7aUJBQ2Y7YUFDSjs7S0FDSjtJQUVELE1BQU0sQ0FBTyxpQ0FBaUMsQ0FBQyxFQUFVLEVBQUcsU0FBbUI7O1lBQzNFLE1BQU0sS0FBSyxHQUFHLE1BQU0sMEJBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMxRCxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sMEJBQWUsQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFBO1lBQ3hELElBQUcsS0FBSyxJQUFHLFNBQVMsSUFBSSxLQUFLLElBQUcsSUFBSSxFQUFFO2dCQUNsQyxTQUFTLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUN0QixJQUFHLENBQUEsS0FBSyxhQUFMLEtBQUssdUJBQUwsS0FBSyxDQUFFLFdBQVcsS0FBSSxTQUFTLElBQUksQ0FBQSxLQUFLLGFBQUwsS0FBSyx1QkFBTCxLQUFLLENBQUUsV0FBVyxLQUFHLElBQUksRUFBQztvQkFDNUQsS0FBSSxNQUFNLFdBQVcsSUFBSSxLQUFLLGFBQUwsS0FBSyx1QkFBTCxLQUFLLENBQUUsV0FBVyxFQUFFO3dCQUN6QyxPQUFPLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFBO3dCQUNuQixNQUFNLElBQUksQ0FBQyxpQ0FBaUMsQ0FBQyxXQUFXLEVBQUMsU0FBUyxDQUFFLENBQUM7cUJBQ3hFO2lCQUNKO2FBQ0o7WUFDRCxPQUFPLFNBQVMsQ0FBQztRQUVyQixDQUFDO0tBQUE7SUFFTSxNQUFNLENBQU8sWUFBWSxDQUFDLEVBQVU7O1lBQ3ZDLE9BQU8sTUFBTSxJQUFJLENBQUMsaUNBQWlDLENBQUMsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBQzlELENBQUM7S0FBQTtDQUdOO0FBOUpELCtCQThKQyJ9