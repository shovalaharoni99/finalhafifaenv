"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const person_repository_1 = __importDefault(require("./../repositories/person.repository"));
class PersonManager {
    constructor() { }
    static getAll() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield person_repository_1.default.getAll();
        });
    }
    static findById(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield person_repository_1.default.findById(id);
        });
    }
    static create(person) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield person_repository_1.default.create(person);
        });
    }
    static deleteOne(id) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield person_repository_1.default.deleteOne(id);
        });
    }
    static update(id, person) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield person_repository_1.default.update(id, person);
        });
    }
}
exports.default = PersonManager;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyc29uLm1hbmFnZXIuanMiLCJzb3VyY2VSb290IjoiLi9zcmMvIiwic291cmNlcyI6WyJtYW5hZ2Vycy9wZXJzb24ubWFuYWdlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUNBLDRGQUFtRTtBQUVuRSxNQUFxQixhQUFhO0lBQzlCLGdCQUFjLENBQUM7SUFDZixNQUFNLENBQU8sTUFBTTs7WUFFZixPQUFPLE1BQU8sMkJBQWdCLENBQUMsTUFBTSxFQUFFLENBQUM7UUFDNUMsQ0FBQztLQUFBO0lBRUQsTUFBTSxDQUFPLFFBQVEsQ0FBRSxFQUFVOztZQUM3QixPQUFPLE1BQU8sMkJBQWdCLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2hELENBQUM7S0FBQTtJQUVELE1BQU0sQ0FBTyxNQUFNLENBQUUsTUFBZTs7WUFDaEMsT0FBTyxNQUFNLDJCQUFnQixDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqRCxDQUFDO0tBQUE7SUFFRCxNQUFNLENBQU8sU0FBUyxDQUFFLEVBQVU7O1lBQzlCLE9BQU8sTUFBTSwyQkFBZ0IsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDaEQsQ0FBQztLQUFBO0lBRUQsTUFBTSxDQUFPLE1BQU0sQ0FBRSxFQUFVLEVBQUUsTUFBYzs7WUFDM0MsT0FBTyxNQUFNLDJCQUFnQixDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsTUFBTSxDQUFDLENBQUM7UUFDcEQsQ0FBQztLQUFBO0NBQ0o7QUF0QkQsZ0NBc0JDIn0=