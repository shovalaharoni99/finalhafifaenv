"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const mongoose = require("mongoose");
const person_manager_1 = __importDefault(require("../managers/person.manager"));
require("mocha");
describe('Person Manager', function () {
    const person = {
        _id: "5e9f105fe41231402cd1f362",
        name: 'shoval',
    };
    before(function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield mongoose.connect('mongodb://127.0.0.1:27017/local', { useNewUrlParser: true });
        });
    });
    afterEach(function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield mongoose.connection.dropDatabase();
        });
    });
    after(function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield mongoose.connection.close();
        });
    });
    describe('#crfud()', function () {
        context('When person is valid', function () {
            it('Should create a new person', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    chai_1.expect(yield person_manager_1.default.create(person)).to.exist;
                });
            });
        });
        context('find person by id', function () {
            it('Should find the person', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    yield person_manager_1.default.create(person);
                    chai_1.expect(yield person_manager_1.default.findById(person._id)).to.exist;
                });
            });
        });
        context('delete person by id', function () {
            it('Should delete the person', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    yield person_manager_1.default.create(person);
                    chai_1.expect(yield person_manager_1.default.deleteOne(person._id)).to.not.exist;
                });
            });
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyc29uLm1hbmFnZXIuc3BlYy5qcyIsInNvdXJjZVJvb3QiOiIuL3NyYy8iLCJzb3VyY2VzIjpbInRlc3QvcGVyc29uLm1hbmFnZXIuc3BlYy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLCtCQUE4QjtBQUM5QixxQ0FBc0M7QUFHdEMsZ0ZBQXVEO0FBQ3ZELGlCQUFlO0FBRWYsUUFBUSxDQUFDLGdCQUFnQixFQUFFO0lBRXZCLE1BQU0sTUFBTSxHQUFHO1FBQ1gsR0FBRyxFQUFFLDBCQUEwQjtRQUMvQixJQUFJLEVBQUUsUUFBUTtLQUNqQixDQUFDO0lBRUYsTUFBTSxDQUFDOztZQUNILE1BQU0sUUFBUSxDQUFDLE9BQU8sQ0FBQyxpQ0FBaUMsRUFBRSxFQUFFLGVBQWUsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQ3pGLENBQUM7S0FBQSxDQUFDLENBQUM7SUFFRixTQUFTLENBQUM7O1lBQ04sTUFBTSxRQUFRLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQzdDLENBQUM7S0FBQSxDQUFDLENBQUM7SUFFSixLQUFLLENBQUM7O1lBQ0YsTUFBTSxRQUFRLENBQUMsVUFBVSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3RDLENBQUM7S0FBQSxDQUFDLENBQUM7SUFFSCxRQUFRLENBQUMsVUFBVSxFQUFFO1FBQ2pCLE9BQU8sQ0FBQyxzQkFBc0IsRUFBRTtZQUM1QixFQUFFLENBQUMsNEJBQTRCLEVBQUU7O29CQUM3QixhQUFNLENBQUMsTUFBTSx3QkFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7Z0JBRXhELENBQUM7YUFBQSxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxtQkFBbUIsRUFBRTtZQUN6QixFQUFFLENBQUMsd0JBQXdCLEVBQUU7O29CQUN6QixNQUFNLHdCQUFhLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO29CQUNuQyxhQUFNLENBQUMsTUFBTSx3QkFBYSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO2dCQUM5RCxDQUFDO2FBQUEsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMscUJBQXFCLEVBQUU7WUFDM0IsRUFBRSxDQUFDLDBCQUEwQixFQUFFOztvQkFDM0IsTUFBTSx3QkFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDbkMsYUFBTSxDQUFFLE1BQU0sd0JBQWEsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7Z0JBQ3BFLENBQUM7YUFBQSxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztJQUVQLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyxDQUFDLENBQUMifQ==