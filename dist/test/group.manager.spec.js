"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const chai_1 = require("chai");
const mongoose = require("mongoose");
const group_manager_1 = __importDefault(require("../managers/group.manager"));
const person_manager_1 = __importDefault(require("../managers/person.manager"));
require("mocha");
describe('Group Manager', function () {
    const person = {
        _id: "5e9f105fe41231402cd1f362",
        name: 'shoval',
    };
    const group = {
        id: "",
        childGroups: [],
        name: 'sport',
    };
    const dupGroups = {
        id: "",
        childGroups: ['1'],
        name: 'food',
    };
    const groupContainsItSelf = {
        id: "",
        name: 'shoham',
        childGroups: []
    };
    before(function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield mongoose.connect('mongodb://127.0.0.1:27017/local', { useNewUrlParser: true });
        });
    });
    afterEach(function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield mongoose.connection.dropDatabase();
        });
    });
    after(function () {
        return __awaiter(this, void 0, void 0, function* () {
            yield mongoose.connection.close();
        });
    });
    describe('#crud()', function () {
        context('search person in group by his name', function () {
            it('should find the person', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    const personCreate = yield person_manager_1.default.create(person);
                    if ((personCreate === null || personCreate === void 0 ? void 0 : personCreate._id) != undefined) {
                        const createdGroup = yield group_manager_1.default.create({ id: "", name: "shoham", person: [personCreate === null || personCreate === void 0 ? void 0 : personCreate._id] });
                        if ((createdGroup === null || createdGroup === void 0 ? void 0 : createdGroup.id) != undefined) {
                            const searchedPerson = yield group_manager_1.default.findPersonInsideGroup(createdGroup === null || createdGroup === void 0 ? void 0 : createdGroup.id, personCreate.name);
                            chai_1.expect(searchedPerson).to.exist;
                        }
                    }
                    else {
                        chai_1.expect.fail;
                    }
                });
            });
        });
        context('When the group contains itself ', function () {
            it('Should throw Error', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    let hasThrown = false;
                    try {
                        let createdGroup = yield group_manager_1.default.create(groupContainsItSelf);
                        yield group_manager_1.default.addChild(groupContainsItSelf.id, groupContainsItSelf.id);
                    }
                    catch (err) {
                        hasThrown = true;
                        chai_1.expect(err).to.exist;
                    }
                    finally {
                        chai_1.expect(hasThrown).to.be.true;
                    }
                });
            });
        });
        context('When the group has duplicate groups ', function () {
            it('Should throw Error', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    let hasThrown = false;
                    try {
                        yield group_manager_1.default.create(group);
                        yield group_manager_1.default.create(dupGroups);
                        yield group_manager_1.default.addChild(dupGroups.id, group.id);
                    }
                    catch (err) {
                        hasThrown = true;
                        chai_1.expect(err).to.exist;
                    }
                    finally {
                        chai_1.expect(hasThrown).to.be.true;
                    }
                });
            });
        });
        context('When group is valid', function () {
            it('should create a new group', function () {
                return __awaiter(this, void 0, void 0, function* () {
                    chai_1.expect(yield group_manager_1.default.create(group)).to.exist;
                });
            });
        });
    });
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAubWFuYWdlci5zcGVjLmpzIiwic291cmNlUm9vdCI6Ii4vc3JjLyIsInNvdXJjZXMiOlsidGVzdC9ncm91cC5tYW5hZ2VyLnNwZWMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7QUFBQSwrQkFBOEI7QUFDOUIscUNBQXNDO0FBR3RDLDhFQUFxRDtBQUNyRCxnRkFBdUQ7QUFDdkQsaUJBQWU7QUFFZixRQUFRLENBQUMsZUFBZSxFQUFFO0lBQ3RCLE1BQU0sTUFBTSxHQUFZO1FBQ3BCLEdBQUcsRUFBRSwwQkFBMEI7UUFDL0IsSUFBSSxFQUFFLFFBQVE7S0FDakIsQ0FBQztJQUNGLE1BQU0sS0FBSyxHQUFXO1FBQ2xCLEVBQUUsRUFBRSxFQUFFO1FBQ04sV0FBVyxFQUFFLEVBQUU7UUFDZixJQUFJLEVBQUUsT0FBTztLQUVoQixDQUFDO0lBQ0YsTUFBTSxTQUFTLEdBQVc7UUFDdEIsRUFBRSxFQUFDLEVBQUU7UUFDTCxXQUFXLEVBQUUsQ0FBQyxHQUFHLENBQUM7UUFDbEIsSUFBSSxFQUFFLE1BQU07S0FDZixDQUFDO0lBQ0YsTUFBTSxtQkFBbUIsR0FBVTtRQUMvQixFQUFFLEVBQUMsRUFBRTtRQUNMLElBQUksRUFBRyxRQUFRO1FBQ2YsV0FBVyxFQUFDLEVBQUU7S0FDakIsQ0FBQztJQUlGLE1BQU0sQ0FBQzs7WUFDSCxNQUFNLFFBQVEsQ0FBQyxPQUFPLENBQUMsaUNBQWlDLEVBQUUsRUFBRSxlQUFlLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQztRQUN6RixDQUFDO0tBQUEsQ0FBQyxDQUFDO0lBRUYsU0FBUyxDQUFDOztZQUNOLE1BQU0sUUFBUSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztRQUM3QyxDQUFDO0tBQUEsQ0FBQyxDQUFDO0lBRUosS0FBSyxDQUFDOztZQUNGLE1BQU0sUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN0QyxDQUFDO0tBQUEsQ0FBQyxDQUFDO0lBRUgsUUFBUSxDQUFDLFNBQVMsRUFBRTtRQUVoQixPQUFPLENBQUMsb0NBQW9DLEVBQUM7WUFDekMsRUFBRSxDQUFDLHdCQUF3QixFQUFDOztvQkFDeEIsTUFBTSxZQUFZLEdBQUcsTUFBTSx3QkFBYSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztvQkFDeEQsSUFBSSxDQUFBLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxHQUFHLEtBQUUsU0FBUyxFQUFDO3dCQUM3QixNQUFNLFlBQVksR0FBRyxNQUFNLHVCQUFZLENBQUMsTUFBTSxDQUFDLEVBQUMsRUFBRSxFQUFDLEVBQUUsRUFBQyxJQUFJLEVBQUMsUUFBUSxFQUFDLE1BQU0sRUFBQyxDQUFDLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxHQUFHLENBQUMsRUFBQyxDQUFDLENBQUM7d0JBQ2pHLElBQUcsQ0FBQSxZQUFZLGFBQVosWUFBWSx1QkFBWixZQUFZLENBQUUsRUFBRSxLQUFHLFNBQVMsRUFBQzs0QkFDNUIsTUFBTSxjQUFjLEdBQUUsTUFBTSx1QkFBWSxDQUFDLHFCQUFxQixDQUFDLFlBQVksYUFBWixZQUFZLHVCQUFaLFlBQVksQ0FBRSxFQUFFLEVBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNuRyxhQUFNLENBQUMsY0FBYyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQzt5QkFDbkM7cUJBQ0o7eUJBQUk7d0JBQ0QsYUFBTSxDQUFDLElBQUksQ0FBQztxQkFDZjtnQkFFTCxDQUFDO2FBQUEsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxDQUFDLENBQUE7UUFDRixPQUFPLENBQUMsaUNBQWlDLEVBQUU7WUFDdkMsRUFBRSxDQUFDLG9CQUFvQixFQUFFOztvQkFDckIsSUFBSSxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN0QixJQUFJO3dCQUNBLElBQUksWUFBWSxHQUFFLE1BQU0sdUJBQVksQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsQ0FBQzt3QkFDakUsTUFBTSx1QkFBWSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxFQUFFLEVBQUMsbUJBQW1CLENBQUMsRUFBRSxDQUFDLENBQUE7cUJBQzdFO29CQUFDLE9BQU8sR0FBRyxFQUFFO3dCQUNWLFNBQVMsR0FBRyxJQUFJLENBQUM7d0JBQ2pCLGFBQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsS0FBSyxDQUFDO3FCQUN4Qjs0QkFBUzt3QkFDTixhQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUM7cUJBQ2hDO2dCQUVMLENBQUM7YUFBQSxDQUFDLENBQUM7UUFDUCxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxzQ0FBc0MsRUFBRTtZQUM1QyxFQUFFLENBQUMsb0JBQW9CLEVBQUU7O29CQUNyQixJQUFJLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQ3RCLElBQUk7d0JBQ0EsTUFBTSx1QkFBWSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQzt3QkFDakMsTUFBTSx1QkFBWSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQzt3QkFDckMsTUFBTSx1QkFBWSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsRUFBRSxFQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztxQkFDdEQ7b0JBQUMsT0FBTyxHQUFHLEVBQUU7d0JBQ1YsU0FBUyxHQUFHLElBQUksQ0FBQzt3QkFDakIsYUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxLQUFLLENBQUM7cUJBQ3hCOzRCQUFTO3dCQUNOLGFBQU0sQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLElBQUksQ0FBQztxQkFDaEM7Z0JBRUwsQ0FBQzthQUFBLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLHFCQUFxQixFQUFFO1lBQzNCLEVBQUUsQ0FBQywyQkFBMkIsRUFBRTs7b0JBQzVCLGFBQU0sQ0FBQyxNQUFNLHVCQUFZLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEtBQUssQ0FBQztnQkFDdEQsQ0FBQzthQUFBLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQyxDQUFDLENBQUM7QUFDUCxDQUFDLENBQUMsQ0FBQyJ9