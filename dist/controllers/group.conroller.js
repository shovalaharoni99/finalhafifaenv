"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.GroupController = void 0;
const group_manager_1 = __importDefault(require("./../managers/group.manager"));
let GroupController = /** @class */ (() => {
    class GroupController {
    }
    GroupController.getAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const groups = yield group_manager_1.default.getAll();
            res.send(groups);
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.findById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.findById(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.create(req.body));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.deleteOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.deleteOne(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.update(req.params.id, req.body));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.addChild = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.addChild(req.params.id, req.body.childGroups));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.newMember = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.newMember(req.params.id, req.body.person));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.getAllChildGroups = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.getAllChildGroups(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.deleteGroup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.deleteGroup(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.findPersonInsideGroup = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.findPersonInsideGroup(req.params.id, req.body.name));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.getAllGroupsOfPerson = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.getAllGroupsOfPerson(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    GroupController.getHierarchy = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield group_manager_1.default.getHierarchy(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    return GroupController;
})();
exports.GroupController = GroupController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZ3JvdXAuY29ucm9sbGVyLmpzIiwic291cmNlUm9vdCI6Ii4vc3JjLyIsInNvdXJjZXMiOlsiY29udHJvbGxlcnMvZ3JvdXAuY29ucm9sbGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7OztBQUNBLGdGQUF1RDtBQUd2RDtJQUFBLE1BQWEsZUFBZTs7SUFDakIsc0JBQU0sR0FBRyxDQUFPLEdBQVksRUFBRSxHQUFhLEVBQUUsRUFBRTtRQUNsRCxJQUFHO1lBQ0MsTUFBTSxNQUFNLEdBQUUsTUFBTSx1QkFBWSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDcEI7UUFBQSxPQUFNLEdBQUcsRUFBQztZQUNQLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQ3hCO0lBQ0wsQ0FBQyxDQUFBLENBQUE7SUFFTSx3QkFBUSxHQUFHLENBQU8sR0FBVyxFQUFFLEdBQVksRUFBRSxFQUFFO1FBQ2xELElBQUc7WUFDQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sdUJBQVksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3hEO1FBQUEsT0FBTSxHQUFHLEVBQUM7WUFDUCxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUN4QjtJQUNMLENBQUMsQ0FBQSxDQUFBO0lBRU0sc0JBQU0sR0FBRyxDQUFPLEdBQVcsRUFBRSxHQUFZLEVBQUMsRUFBRTtRQUMvQyxJQUFHO1lBQ0MsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLHVCQUFZLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ2pEO1FBQUEsT0FBTSxHQUFHLEVBQUM7WUFDUCxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUN4QjtJQUNMLENBQUMsQ0FBQSxDQUFBO0lBRU0seUJBQVMsR0FBRyxDQUFPLEdBQVcsRUFBRSxHQUFZLEVBQUUsRUFBRTtRQUNuRCxJQUFHO1lBQ0MsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLHVCQUFZLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUN6RDtRQUFBLE9BQU0sR0FBRyxFQUFDO1lBQ1AsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDeEI7SUFDTCxDQUFDLENBQUEsQ0FBQTtJQUVNLHNCQUFNLEdBQUcsQ0FBTyxHQUFXLEVBQUUsR0FBWSxFQUFFLEVBQUU7UUFDaEQsSUFBRztZQUNDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSx1QkFBWSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNoRTtRQUFBLE9BQU0sR0FBRyxFQUFDO1lBQ1AsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDeEI7SUFDTCxDQUFDLENBQUEsQ0FBQTtJQUVNLHdCQUFRLEdBQUcsQ0FBTyxHQUFXLEVBQUUsR0FBWSxFQUFFLEVBQUU7UUFDbEQsSUFBRztZQUNDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSx1QkFBWSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7U0FDN0U7UUFBQSxPQUFNLEdBQUcsRUFBQztZQUNQLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQyxDQUFBLENBQUE7SUFHTSx5QkFBUyxHQUFJLENBQU8sR0FBVyxFQUFFLEdBQVksRUFBRSxFQUFFO1FBQ3BELElBQUc7WUFDQyxHQUFHLENBQUMsSUFBSSxDQUFFLE1BQU0sdUJBQVksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQzFFO1FBQUEsT0FBTSxHQUFHLEVBQUM7WUFDUCxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUN6QjtJQUNMLENBQUMsQ0FBQSxDQUFBO0lBRU0saUNBQWlCLEdBQUcsQ0FBTyxHQUFXLEVBQUUsR0FBWSxFQUFFLEVBQUU7UUFDM0QsSUFBRztZQUNDLEdBQUcsQ0FBQyxJQUFJLENBQUUsTUFBTSx1QkFBWSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNsRTtRQUFBLE9BQU0sR0FBRyxFQUFDO1lBQ1AsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDekI7SUFDTCxDQUFDLENBQUEsQ0FBQTtJQUVNLDJCQUFXLEdBQUcsQ0FBTyxHQUFXLEVBQUUsR0FBWSxFQUFFLEVBQUU7UUFDckQsSUFBRztZQUNDLEdBQUcsQ0FBQyxJQUFJLENBQUUsTUFBTSx1QkFBWSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDNUQ7UUFBQSxPQUFNLEdBQUcsRUFBQztZQUNQLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQyxDQUFBLENBQUE7SUFFTSxxQ0FBcUIsR0FBRyxDQUFPLEdBQVcsRUFBRSxHQUFZLEVBQUUsRUFBRTtRQUMvRCxJQUFHO1lBQ0MsR0FBRyxDQUFDLElBQUksQ0FBRSxNQUFNLHVCQUFZLENBQUMscUJBQXFCLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ3BGO1FBQUEsT0FBTSxHQUFHLEVBQUM7WUFDUCxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUN6QjtJQUNMLENBQUMsQ0FBQSxDQUFBO0lBRU0sb0NBQW9CLEdBQUcsQ0FBTyxHQUFXLEVBQUUsR0FBWSxFQUFFLEVBQUU7UUFDOUQsSUFBRztZQUNDLEdBQUcsQ0FBQyxJQUFJLENBQUUsTUFBTSx1QkFBWSxDQUFDLG9CQUFvQixDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUNyRTtRQUFBLE9BQU0sR0FBRyxFQUFDO1lBQ1AsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDeEI7SUFDTCxDQUFDLENBQUEsQ0FBQTtJQUVPLDRCQUFZLEdBQUUsQ0FBTyxHQUFXLEVBQUUsR0FBWSxFQUFFLEVBQUU7UUFDdEQsSUFBRztZQUNDLEdBQUcsQ0FBQyxJQUFJLENBQUUsTUFBTSx1QkFBWSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDN0Q7UUFBQSxPQUFNLEdBQUcsRUFBQztZQUNQLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQ3hCO0lBQ0wsQ0FBQyxDQUFBLENBQUE7SUFHTCxzQkFBQztLQUFBO0FBcEdZLDBDQUFlIn0=