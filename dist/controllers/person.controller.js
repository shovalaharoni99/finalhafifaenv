"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonController = void 0;
const person_manager_1 = __importDefault(require("./../managers/person.manager"));
let PersonController = /** @class */ (() => {
    class PersonController {
    }
    PersonController.getAll = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            const persons = yield person_manager_1.default.getAll();
            res.send(persons);
        }
        catch (err) {
            res.send(err.message);
        }
    });
    PersonController.findById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield person_manager_1.default.findById(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    PersonController.create = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield person_manager_1.default.create(req.body));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    PersonController.deleteOne = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield person_manager_1.default.deleteOne(req.params.id));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    PersonController.update = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
        try {
            res.send(yield person_manager_1.default.update(req.params.id, req.body));
        }
        catch (err) {
            res.send(err.message);
        }
    });
    return PersonController;
})();
exports.PersonController = PersonController;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVyc29uLmNvbnRyb2xsZXIuanMiLCJzb3VyY2VSb290IjoiLi9zcmMvIiwic291cmNlcyI6WyJjb250cm9sbGVycy9wZXJzb24uY29udHJvbGxlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7QUFDQSxrRkFBeUQ7QUFHekQ7SUFBQSxNQUFhLGdCQUFnQjs7SUFDbEIsdUJBQU0sR0FBRyxDQUFPLEdBQVksRUFBRSxHQUFhLEVBQUUsRUFBRTtRQUNsRCxJQUFHO1lBQ0MsTUFBTSxPQUFPLEdBQUUsTUFBTSx3QkFBYSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzVDLEdBQUcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFHLENBQUM7U0FDdkI7UUFBQSxPQUFNLEdBQUcsRUFBQztZQUNQLEdBQUcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFBO1NBQ3hCO0lBQ0wsQ0FBQyxDQUFBLENBQUE7SUFFTSx5QkFBUSxHQUFHLENBQU8sR0FBVyxFQUFFLEdBQVksRUFBRSxFQUFFO1FBQ2xELElBQUc7WUFDQyxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sd0JBQWEsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ3pEO1FBQUEsT0FBTSxHQUFHLEVBQUM7WUFDUCxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUN4QjtJQUNMLENBQUMsQ0FBQSxDQUFBO0lBRU0sdUJBQU0sR0FBRyxDQUFPLEdBQVcsRUFBRSxHQUFZLEVBQUMsRUFBRTtRQUMvQyxJQUFHO1lBQ0MsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLHdCQUFhLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ2xEO1FBQUEsT0FBTSxHQUFHLEVBQUM7WUFDUCxHQUFHLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQTtTQUN4QjtJQUNMLENBQUMsQ0FBQSxDQUFBO0lBRU0sMEJBQVMsR0FBRyxDQUFPLEdBQVcsRUFBRSxHQUFZLEVBQUUsRUFBRTtRQUNuRCxJQUFHO1lBQ0MsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLHdCQUFhLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztTQUMxRDtRQUFBLE9BQU0sR0FBRyxFQUFDO1lBQ1AsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDeEI7SUFDTCxDQUFDLENBQUEsQ0FBQTtJQUVNLHVCQUFNLEdBQUcsQ0FBTyxHQUFXLEVBQUUsR0FBWSxFQUFFLEVBQUU7UUFDaEQsSUFBRztZQUNDLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSx3QkFBYSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNqRTtRQUFBLE9BQU0sR0FBRyxFQUFDO1lBQ1AsR0FBRyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUE7U0FDeEI7SUFDTCxDQUFDLENBQUEsQ0FBQTtJQUNMLHVCQUFDO0tBQUE7QUF6Q1ksNENBQWdCIn0=